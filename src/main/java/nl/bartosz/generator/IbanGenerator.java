package nl.bartosz.generator;

import java.math.BigInteger;
import java.util.Random;

/**
 * Created by pascal on 01-01-16.
 *
 * https://nl.wikipedia.org/wiki/International_Bank_Account_Number
 *
 */
public class IbanGenerator {

    // Range for the accountNumberGenerator
    private static final int LOW = 100000000;
    private static final int HIGH = 999999900;


    /**
     * Ik genereer een  IBAN for numerieke rekeningnummers tussen 10 en 30 karakters lang.
     *
     * https://nl.wikipedia.org/wiki/International_Bank_Account_Number
     *
     * @param countryCode alleen NL of DK wordt ondersteund, er zijn veel verschillende IBAN formaten
     * @param bankCode 4 alpha karakters
     * @param accountNumber 10 - 30 numerical characters, for NL or DK length = 10
     * @return IBAN number as String
     */
    public static String generateIBAN(String countryCode, String bankCode, String accountNumber) throws Exception {

        if(accountNumber.length() < 10 && accountNumber.length() > 30){
            System.out.println("The accountnumber needs to be between 10 and 30 number character long");
        }
        BigInteger numberToCheck = new BigInteger(getNumericValuesFromString(bankCode) + accountNumber + getNumericValuesFromString(countryCode) + "00");
        BigInteger modulusValue = new BigInteger("97");
        int remainder = numberToCheck.mod(modulusValue).intValue();
        String controlValue = String.valueOf(98 - remainder);

        if(controlValue.length()<2){
            return countryCode + "0" + controlValue + bankCode + accountNumber;
        } else return countryCode + controlValue + bankCode + accountNumber;
    }

    public static String generateIBAN(String countryCode, String bankCode) throws Exception {
        String accountNumber = generateAccountNumber();
        return generateIBAN(countryCode,bankCode,accountNumber);
    }

    /**
     * Ik valideer een Nederlands IBAN van 18 karakters lang. Ik forceer de input van landcode NL niet.
     * Format NLkk BBBB CCCC CCCC CK:
     *   NL countryCode
     *   kk controlValue
     *   BBBB bankCode
     *   CCCC CCCC CK accountNumber (including controlvalue)
     *
     * @param iban
     * @return true / false
     */
    public static Boolean isDutchIbanValid(String iban) throws Exception {
        if(iban.length() != 18){
            throw new IllegalArgumentException("Supplied IBAN is not 18 character long.");
        }

        String countryCode = iban.substring(0, 2);
        String controlValue = iban.substring(2, 4);
        String bankCode = iban.substring(4, 8);
        String accountNumber = iban.substring(8);

        BigInteger numberToCheck = new BigInteger(getNumericValuesFromString(bankCode) + accountNumber + getNumericValuesFromString(countryCode) + controlValue);
        BigInteger modulusValue = new BigInteger("97");

        return (numberToCheck.mod(modulusValue).intValue() == 1);
    }

    /**
     *  Ik geef de numerieke waardes van de karakters in een string terug. Ik accepteer alleen strings van 2 of 4 karakters.
     *
     * @param string
     * @return
     * @throws Exception
     */
    public static String getNumericValuesFromString(String string) throws Exception {

        if (string.length() == 4){
            String bc1 = string.substring(0,1);
            String bc2 = string.substring(1,2);
            String bc3 = string.substring(2,3);
            String bc4 = string.substring(3,4);

            return getNumericValueFromCharacter(bc1) + getNumericValueFromCharacter(bc2) + getNumericValueFromCharacter(bc3) + getNumericValueFromCharacter(bc4);
        }

        if (string.length() == 2){
            String cc1 = string.substring(0,1);
            String cc2 = string.substring(1,2);

            return getNumericValueFromCharacter(cc1) + getNumericValueFromCharacter(cc2);
        } else
            throw new IllegalArgumentException("This only works with a BankCode of 4 characters or a Countrycode of 2 characters, you supplied " + string.length() + " characters");

    }

    /**
     * Ik geef de numerieke waarde van een karakter terug
     *
     * @param string
     * @return
     */
    public static String getNumericValueFromCharacter(String string){
        return String.valueOf(Character.getNumericValue(string.toLowerCase().charAt(0)));
    }

    /**
     *  Ik genereer een rekening nummer dat voldoet aan de ElfProef voor Nederlandse Bankrekeningnummers
     *  Ik genereer nummers in een bepaalde range, tussen 100000000 en 999999900
     *  https://nl.wikipedia.org/wiki/Elfproef
     *
     * @return
     * @throws Exception
     */
    public static String generateAccountNumber() {
        int possibleAccountNumber = giveRandomStartingNumber();

        for (;;++possibleAccountNumber) {
            if (checkElfProef(possibleAccountNumber)) {
                return "0" + String.valueOf(possibleAccountNumber);
            }

            if (possibleAccountNumber > HIGH) {
                System.out.println("No accountNumber was found.");
            }
        }
    }

    /**
     * Ik genereer een random getal tussen twee gespecificeerde getallen
     * @return
     */
    public static int giveRandomStartingNumber() {
        return new Random().nextInt(HIGH - LOW) + LOW;
    }


    /**
     * Ik controleer of een nummer dat aan mij wordt gegeven voldoet aan de EldProef voor Nederlandse bankrekeningnummers
     * https://nl.wikipedia.org/wiki/Elfproef
     *
     * @param possibleAccountNumber
     * @return
     * @throws Exception
     */
    public static boolean checkElfProef(int possibleAccountNumber) {
        if (giveLengthOfAccountNumber(possibleAccountNumber) == 9){
            int A, B, C, D, E, F, G, H, I, pos = 0;

            A = giveNumberOnPosition(possibleAccountNumber, pos++);
            B = giveNumberOnPosition(possibleAccountNumber, pos++);
            C = giveNumberOnPosition(possibleAccountNumber, pos++);
            D = giveNumberOnPosition(possibleAccountNumber, pos++);
            E = giveNumberOnPosition(possibleAccountNumber, pos++);
            F = giveNumberOnPosition(possibleAccountNumber, pos++);
            G = giveNumberOnPosition(possibleAccountNumber, pos++);
            H = giveNumberOnPosition(possibleAccountNumber, pos++);
            I = giveNumberOnPosition(possibleAccountNumber, pos);

            int resultaat = (9 * A) + (8 * B) + (7 * C) + (6 * D) + (5 * E) + (4 * F) + (3 * G) + (2 * H) + (1 * I);

            return resultaat % 11 == 0;
        }
        else throw new IllegalArgumentException("Het opgegeven nummer bevat geen 9 karakters");
    }

    public static int giveLengthOfAccountNumber(int possibleAccountNumber) {
        return String.valueOf(possibleAccountNumber).length();
    }

    /**
     * Ik geef het nummer dat op een bepaalde positie staat in de input terug
     *
     * @param input
     * @param position
     * @return
     */
    public static int giveNumberOnPosition(int input, int position) {
        String number = String.valueOf(input);
        String substring = number.substring(position, position + 1);
        return Integer.parseInt(substring);
    }

}
