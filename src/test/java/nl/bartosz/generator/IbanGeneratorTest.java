package nl.bartosz.generator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class IbanGeneratorTest {

    // Implement the function that returns the length of of the given number
    @Test
    public void testGetLengthOfAccountNumber() {
        assertEquals(1, IbanGenerator.giveLengthOfAccountNumber(1));
        assertEquals(5, IbanGenerator.giveLengthOfAccountNumber(12345));
        assertEquals(7, IbanGenerator.giveLengthOfAccountNumber(1234567));
        assertEquals(10, IbanGenerator.giveLengthOfAccountNumber(1234567890));
    }

    // Implement the giveNumberOnPosition function that returns a single digit at the specified position of the original number.
    @Test
    public void testGiveNumberOnPosition() throws Exception {
        int number = 237949502;
        assertEquals(2, IbanGenerator.giveNumberOnPosition(number, 0));
        assertEquals(3, IbanGenerator.giveNumberOnPosition(number, 1));
        assertEquals(7, IbanGenerator.giveNumberOnPosition(number, 2));
        assertEquals(9, IbanGenerator.giveNumberOnPosition(number, 3));
        assertEquals(4, IbanGenerator.giveNumberOnPosition(number, 4));
        assertEquals(9, IbanGenerator.giveNumberOnPosition(number, 5));
        assertEquals(5, IbanGenerator.giveNumberOnPosition(number, 6));
        assertEquals(0, IbanGenerator.giveNumberOnPosition(number, 7));
        assertEquals(2, IbanGenerator.giveNumberOnPosition(number, 8));
    }

    // Implement a method that returns a random number between 100000000 and 999999900 (inclusive)
    @Test
    public void testGiveRandomStartingNumber() {
        int number = IbanGenerator.giveRandomStartingNumber();
        assertTrue(number >= 100000000);
        assertTrue(number <= 999999900);
    }

    // For our IBAN validation we need to map the characters to a corresponding number where A = 10, B = 11, etc.
    @Test
    public void testGetNumericValueFromCharacter() {
        assertEquals("10", IbanGenerator.getNumericValueFromCharacter("A"));
        assertEquals("11", IbanGenerator.getNumericValueFromCharacter("B"));
        assertEquals("12", IbanGenerator.getNumericValueFromCharacter("C"));
        assertEquals("13", IbanGenerator.getNumericValueFromCharacter("D"));

        assertEquals("10", IbanGenerator.getNumericValueFromCharacter("a"));
        assertEquals("11", IbanGenerator.getNumericValueFromCharacter("b"));
        assertEquals("12", IbanGenerator.getNumericValueFromCharacter("c"));
        assertEquals("13", IbanGenerator.getNumericValueFromCharacter("d"));
    }

    // For our IBAN validation we need to calculate the sum of all numbers for the characters used
    @Test
    public void testGetNumericValueFromString() {
        assertEquals("27", IbanGenerator.getNumericValueFromCharacter("RABO"));
        assertEquals("20", IbanGenerator.getNumericValueFromCharacter("KNAB"));
        assertEquals("10", IbanGenerator.getNumericValueFromCharacter("ABNA"));
    }
}